# XTaws Beta Group

## Please select the "Issues" menu on the left to enter (Not visible? Please [Sign In](https://bitbucket.org/account/signin/))

Thank you for joining the XTaws beta group and for helping us iron out any wrinkles.

Feel free to add or comment on as many tickets as you want to. Use these tickets to report bugs, observations, suggestions or questions.

Thanks once again for your invaluable assistance, it is truly appreciated by the team.

# Downloads (Version 1.0 - 12 April 2019)

**Note:** Please always update the main app and the plugin!

## ![win.png](https://bitbucket.org/repo/p4beLko/images/2256158708-win.png) Windows

[XTaws for Windows (64 bit)](http://fswidgets-downloads.s3.amazonaws.com/xtaws_win.zip)
 
[XTaws Plugin for Windows (64 bit)](http://fswidgets-downloads.s3.amazonaws.com/xtaws_plugin.zip)

## ![mac.png](https://bitbucket.org/repo/p4beLko/images/3623323804-mac.png) Mac

[XTaws for Mac](http://fswidgets-downloads.s3.amazonaws.com/xtaws_mac.dmg) (Includes Plugin)
 
## ![doc.png](https://bitbucket.org/repo/p4beLko/images/935847280-doc.png) Documents

[View Latest Changelog](https://www.dropbox.com/s/fi57gydtrw62v4h/changelog.txt?dl=0)

[View Latest User Manual](https://fswidgets-docs.s3.amazonaws.com/xtaws/index.html)
